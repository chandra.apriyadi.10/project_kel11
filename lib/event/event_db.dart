import 'dart:convert';
import 'package:get/get.dart';
import 'package:project_si20b/config/api.dart';
import 'package:project_si20b/controller/c_aset.dart';
import 'package:project_si20b/event/event_pref.dart';
import 'package:project_si20b/model/aset.dart';
import 'package:project_si20b/model/pengembalian.dart';
import 'package:project_si20b/model/peminjaman.dart';
import 'package:project_si20b/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:project_si20b/screen/admin/dashboard_admin.dart';
import 'package:project_si20b/screen/admin/home_screen.dart';
// import 'package:project_si20b/screen/admin/list_user.dart';
import 'package:project_si20b/screen/login.dart';
import 'package:project_si20b/widget/info.dart';

// import '../model/mahasiswa.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              DashboardAdmin(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(String name, String username, String pass,
      String role, String alamat, String prodi, String hp) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role,
        'alamat': alamat,
        'prodi': prodi,
        'hp': hp,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Tambah User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Tambah User Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
    String alamat,
    String prodi,
    String hp,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role,
        'alamat': alamat,
        'prodi': prodi,
        'hp': hp,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  // static Future<List<Mahasiswa>> getMahasiswa() async {
  //   List<Mahasiswa> listMahasiswa = [];

  //   try {
  //     var response = await http.get(Uri.parse(Api.getMahasiswa));

  //     if (response.statusCode == 200) {
  //       var responBody = jsonDecode(response.body);
  //       if (responBody['success']) {
  //         var mahasiswa = responBody['mahasiswa'];

  //         mahasiswa.forEach((mahasiswa) {
  //           listMahasiswa.add(Mahasiswa.fromJson(mahasiswa));
  //         });
  //       }
  //     }
  //   } catch (e) {
  //     print(e);
  //   }

  //   return listMahasiswa;
  // }

  // static Future<String> AddMahasiswa(
  //     String mhsNpm,
  //     String mhsNama,
  //     String mhsAlamat,
  //     String mhsFakultas,
  //     String MhsProdi,
  //     String mhsHp) async {
  //   String reason;

  //   try {
  //     var response = await http.post(Uri.parse(Api.addMahasiswa), body: {
  //       'mhsNpm': mhsNpm,
  //       'mhsNama': mhsNama,
  //       'mhsAlamat': mhsAlamat,
  //       'mhsFakultas': mhsFakultas,
  //       'MhsProdi': MhsProdi,
  //       'mhsHp': mhsHp,
  //     });

  //     if (response.statusCode == 200) {
  //       var responBody = jsonDecode(response.body);
  //       if (responBody['success']) {
  //         reason = 'Add Mahasiswa Berhasil';
  //       } else {
  //         reason = responBody['reason'];
  //       }
  //     } else {
  //       reason = "Add Mahasiswa Gagal";
  //     }
  //   } catch (e) {
  //     print(e);
  //     reason = e.toString();
  //   }

  //   return reason;
  // }

  // static Future<void> UpdateMahasiswa(
  //     String mhsNpm,
  //     String mhsNama,
  //     String mhsAlamat,
  //     String mhsFakultas,
  //     String MhsProdi,
  //     String mhsHp) async {
  //   try {
  //     var response = await http.post(Uri.parse(Api.updateMahasiswa), body: {
  //       'mhsNpm': mhsNpm,
  //       'mhsNama': mhsNama,
  //       'mhsAlamat': mhsAlamat,
  //       'mhsFakultas': mhsFakultas,
  //       'MhsProdi': MhsProdi,
  //       'mhsHp': mhsHp,
  //     });

  //     if (response.statusCode == 200) {
  //       var responBody = jsonDecode(response.body);
  //       if (responBody['success']) {
  //         Info.snackbar('Berhasil Update Mahasiswa');
  //       } else {
  //         Info.snackbar('Gagal Update Mahasiswa');
  //       }
  //     }
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  // static Future<void> deleteMahasiswa(String mhsNpm) async {
  //   try {
  //     var response = await http
  //         .post(Uri.parse(Api.deleteMahasiswa), body: {'mhsNpm': mhsNpm});

  //     if (response.statusCode == 200) {
  //       var responBody = jsonDecode(response.body);
  //       if (responBody['success']) {
  //         Info.snackbar('Berhasil Delete Mahasiswa');
  //       } else {
  //         Info.snackbar('Gagal Delete Mahasiswa');
  //       }
  //     }
  //   } catch (e) {
  //     print(e);
  //   }
  // }

//aset
  static Future<List<Aset>> getAset() async {
    List<Aset> listAset = [];

    try {
      var response = await http.get(Uri.parse(Api.getAset));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var aset = responBody['aset'];

          aset.forEach((aset) {
            listAset.add(Aset.fromJson(aset));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listAset;
  }

  static Future<String> addAset(String namaAset, String jumlahAset) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addAset), body: {
        // 'id_aset': idAset,
        'nama_aset': namaAset,
        'jumlah_aset': jumlahAset,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Tambah Aset Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Tambah Aset Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateAset(
    String idAset,
    String namaAset,
    String jumlahAset,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateAset), body: {
        'id_aset': idAset,
        'nama_aset': namaAset,
        'jumlah_aset': jumlahAset,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Aset');
        } else {
          Info.snackbar('Gagal Update Aset');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteAset(String idAset) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteAset), body: {'id_aset': idAset});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Aset');
        } else {
          Info.snackbar('Gagal Delete Aset');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  // //peminjaman
  static Future<List<Peminjaman>> getPeminjaman() async {
    List<Peminjaman> listPeminjaman = [];

    try {
      var response = await http.get(Uri.parse(Api.getPeminjaman));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var peminjaman = responBody['peminjaman'];

          peminjaman.forEach((peminjaman) {
            listPeminjaman.add(Peminjaman.fromJson(peminjaman));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPeminjaman;
  }

  static Future<String> addPeminjaman(
      String idPeminjaman,
      String idUser,
      String name,
      String idAset,
      String namaAset,
      String tanggalPinjam,
      String persetujuan) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPeminjaman), body: {
        'id_peminjaman': idPeminjaman,
        'id_user': idUser,
        'name': name,
        'id_aset': idAset,
        'nama_aset': namaAset,
        'tanggal_pinjam': tanggalPinjam,
        'persetujuan': persetujuan,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Tambah Peminjaman Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Tambah Peminjaman Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePeminjaman(
      String idPeminjaman,
      String idUser,
      String name,
      String idAset,
      String namaAset,
      String tanggalPinjam,
      String persetujuan) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePeminjaman), body: {
        'id_peminjaman': idPeminjaman,
        'id_user': idUser,
        'name': name,
        'id_aset': idAset,
        'nama_aset': namaAset,
        'tanggal_pinjam': tanggalPinjam,
        'persetujuan': persetujuan,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Peminjaman');
        } else {
          Info.snackbar('Gagal Update Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePeminjaman(String idPeminjaman) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePeminjaman),
          body: {'id_peminjaman': idPeminjaman});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Peminjaman');
        } else {
          Info.snackbar('Gagal Delete Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //pengembalian
  static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['pengembalian'];

          pengembalian.forEach((pengembalian) {
            listPengembalian.add(Pengembalian.fromJson(pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }

  static Future<String> addPengembalian(
      // String idPengembalian,
      String idPeminjaman,
      String idUser,
      String name,
      String tanggalKembali,
      String sudahDikembalikan) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        // 'id_pengembalian': idPengembalian,
        'id_peminjaman': idPeminjaman,
        'id_user': idUser,
        'name': name,
        'tanggal_kembali': tanggalKembali,
        'sudah_dikembalikan': sudahDikembalikan,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Tambah Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Tambah Pengembalian Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengembalian(
    String idPengembalian,
    String idPeminjaman,
    String idUser,
    String name,
    String tanggalKembali,
    String sudahDikembalikan,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'id_pengembalian': idPengembalian,
        'id_peminjaman': idPeminjaman,
        'id_user': idUser,
        'name': name,
        'tanggal_kembali': tanggalKembali,
        'sudah_dikembalikan': sudahDikembalikan,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalian(String idPengembalian) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalian),
          body: {'id_pengembalian': idPengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
