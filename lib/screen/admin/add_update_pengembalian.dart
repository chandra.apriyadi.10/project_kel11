import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/event/event_db.dart';
import 'package:project_si20b/screen/admin/list_pengembalian.dart';
import 'package:project_si20b/widget/info.dart';

import '../../model/pengembalian.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? pengembalian;
  AddUpdatePengembalian({this.pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerIdPengembalian = TextEditingController();
  var _controllerIdPeminjaman = TextEditingController();
  var _controllerIdUser = TextEditingController();
  var _controllerName = TextEditingController();
  var _controllerTanggalKembali = TextEditingController();

  List<String> sudahDikembalikan = [
    "Sudah",
    "Belum",
  ];

  String _sudahDikembalikan = "Sudah";

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian != null) {
      _controllerIdPengembalian.text = widget.pengembalian!.idPengembalian!;
      _controllerIdPeminjaman.text = widget.pengembalian!.idPeminjaman!;
      _controllerIdUser.text = widget.pengembalian!.idUser!;
      _controllerName.text = widget.pengembalian!.name!;
      _controllerTanggalKembali.text = widget.pengembalian!.tanggalKembali!;
      _sudahDikembalikan = widget.pengembalian!.sudahDikembalikan!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Pengembalian'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerIdPeminjaman,
                  decoration: InputDecoration(
                      labelText: "Id Peminjaman",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerIdUser,
                  decoration: InputDecoration(
                      labelText: "Id User",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerName,
                  decoration: InputDecoration(
                      labelText: "Name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggalKembali,
                  decoration: InputDecoration(
                      labelText: "Tanggal Kembali",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField<String>(
                  value: _sudahDikembalikan,
                  decoration: InputDecoration(
                      labelText: "Sudah Dikembalikan ?",
                      hintText: "Sudah Dikembalikan ?",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  items: sudahDikembalikan.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _sudahDikembalikan = newValue!;
                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian == null) {
                        String message = await EventDb.addPengembalian(
                          // _controllerIdPengembalian.text,
                          _controllerIdPeminjaman.text,
                          _controllerIdUser.text,
                          _controllerName.text,
                          _controllerTanggalKembali.text,
                          _sudahDikembalikan,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerIdPeminjaman.clear();
                          _controllerIdUser.clear();
                          _controllerName.clear();
                          _controllerTanggalKembali.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                            _controllerIdPengembalian.text,
                            _controllerIdPeminjaman.text,
                            _controllerIdUser.text,
                            _controllerName.text,
                            _controllerTanggalKembali.text,
                            _sudahDikembalikan);
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
