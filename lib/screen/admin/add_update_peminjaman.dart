import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/event/event_db.dart';
import 'package:project_si20b/screen/admin/list_peminjaman.dart';
import 'package:project_si20b/widget/info.dart';

import '../../model/peminjaman.dart';

class AddUpdatePeminjaman extends StatefulWidget {
  final Peminjaman? peminjaman;
  AddUpdatePeminjaman({this.peminjaman});

  @override
  State<AddUpdatePeminjaman> createState() => _AddUpdatePeminjamanState();
}

class _AddUpdatePeminjamanState extends State<AddUpdatePeminjaman> {
  var _formKey = GlobalKey<FormState>();
  var _controllerIdPeminjaman = TextEditingController();
  var _controllerIdUser = TextEditingController();
  var _controllerName = TextEditingController();
  var _controllerIdAset = TextEditingController();
  var _controllerNamaAset = TextEditingController();
  var _controllerTanggalPinjam = TextEditingController();

  List<String> persetujuan = [
    "Di Setujui",
    "Tidak di Setujui",
  ];

  String _persetujuan = "Di Setujui";

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.peminjaman != null) {
      _controllerIdPeminjaman.text = widget.peminjaman!.idPeminjaman!;
      _controllerIdUser.text = widget.peminjaman!.idUser!;
      _controllerName.text = widget.peminjaman!.name!;
      _controllerIdAset.text = widget.peminjaman!.idAset!;
      _controllerNamaAset.text = widget.peminjaman!.namaAset!;
      _controllerTanggalPinjam.text = widget.peminjaman!.tanggalPinjam!;
      _persetujuan = widget.peminjaman!.persetujuan!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Peminjaman'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerIdUser,
                  decoration: InputDecoration(
                      labelText: "Id User",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerName,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerIdAset,
                  decoration: InputDecoration(
                      labelText: "Id Asset",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNamaAset,
                  decoration: InputDecoration(
                      labelText: "Nama Aset",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggalPinjam,
                  decoration: InputDecoration(
                      labelText: "Tanggal Pinjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField<String>(
                  value: _persetujuan,
                  decoration: InputDecoration(
                      labelText: "Persetujuan",
                      hintText: "Persetujuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  items: persetujuan.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _persetujuan = newValue!;
                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.peminjaman == null) {
                        String message = await EventDb.addPeminjaman(
                          _controllerIdPeminjaman.text,
                          _controllerIdUser.text,
                          _controllerName.text,
                          _controllerIdAset.text,
                          _controllerNamaAset.text,
                          _controllerTanggalPinjam.text,
                          _persetujuan,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerIdUser.clear();
                          _controllerName.clear();
                          _controllerIdAset.clear();
                          _controllerNamaAset.clear();
                          _controllerTanggalPinjam.clear();
                          Get.off(
                            ListPeminjaman(),
                          );
                        }
                      } else {
                        EventDb.UpdatePeminjaman(
                            _controllerIdPeminjaman.text,
                            _controllerIdUser.text,
                            _controllerName.text,
                            _controllerIdAset.text,
                            _controllerNamaAset.text,
                            _controllerTanggalPinjam.text,
                            _persetujuan);
                      }
                    }
                  },
                  child: Text(
                    widget.peminjaman == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
