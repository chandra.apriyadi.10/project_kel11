import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/event/event_db.dart';
import 'package:project_si20b/screen/admin/list_aset.dart';
import 'package:project_si20b/widget/info.dart';

import '../../model/aset.dart';

class AddUpdateAset extends StatefulWidget {
  final Aset? aset;
  AddUpdateAset({this.aset});

  @override
  State<AddUpdateAset> createState() => _AddUpdateAsetState();
}

class _AddUpdateAsetState extends State<AddUpdateAset> {
  var _formKey = GlobalKey<FormState>();
  var _controllerIdAset = TextEditingController();
  var _controllerNamaAset = TextEditingController();
  var _controllerJumlahAset = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.aset != null) {
      _controllerIdAset.text = widget.aset!.idAset!;
      _controllerNamaAset.text = widget.aset!.namaAset!;
      _controllerJumlahAset.text = widget.aset!.jumlahAset!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Aset'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                // TextFormField(
                //   validator: (value) => value == '' ? 'Jangan Kosong' : null,
                //   controller: _controllerIdAset,
                //   decoration: InputDecoration(
                //       labelText: "Id Aset",
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10))),
                // ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNamaAset,
                  decoration: InputDecoration(
                      labelText: "Nama Aset",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerJumlahAset,
                  decoration: InputDecoration(
                      labelText: "Jumlah Aset",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.aset == null) {
                        String message = await EventDb.addAset(
                          // _controllerIdAset.text,
                          _controllerNamaAset.text,
                          _controllerJumlahAset.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          // _controllerIdAset.clear();
                          _controllerNamaAset.clear();
                          _controllerJumlahAset.clear();
                        }
                      } else {
                        EventDb.UpdateAset(
                            _controllerIdAset.text,
                            _controllerNamaAset.text,
                            _controllerJumlahAset.text);
                      }
                    }
                  },
                  child: Text(
                    widget.aset == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
