import 'package:project_si20b/model/user.dart';
import 'package:flutter/material.dart';

class ProfilScreen extends StatefulWidget {
  final User user;

  ProfilScreen({required this.user});

  @override
  _ProfilScreenState createState() => _ProfilScreenState();
}

class _ProfilScreenState extends State<ProfilScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Profile'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('ID User: ${widget.user.idUser}'),
            Text('Nama Lengkap: ${widget.user.name}'),
            Text('Username: ${widget.user.userName}'),
            Text('Status: ${widget.user.role}'),
            Text('Alamat: ${widget.user.alamat}'),
            Text('Prodi: ${widget.user.prodi}'),
            Text('No Handphone: ${widget.user.hp}'),
          ],
        ),
      ),
    );
  }
}
