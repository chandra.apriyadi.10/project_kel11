import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/event/event_db.dart';
import 'package:project_si20b/screen/admin/add_update_peminjaman.dart';

import '../../model/peminjaman.dart';

class ListPeminjaman extends StatefulWidget {
  @override
  State<ListPeminjaman> createState() => _ListPeminjamanState();
}

class _ListPeminjamanState extends State<ListPeminjaman> {
  List<Peminjaman> _listPeminjaman = [];

  void getPeminjaman() async {
    _listPeminjaman = await EventDb.getPeminjaman();

    setState(() {});
  }

  @override
  void initState() {
    getPeminjaman();
    super.initState();
  }

  void showOption(Peminjaman? peminjaman) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePeminjaman(peminjaman: peminjaman))
            ?.then((value) => getPeminjaman());
        break;
      case 'delete':
        EventDb.deletePeminjaman(peminjaman!.idPeminjaman!)
            .then((value) => getPeminjaman());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Peminjaman'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPeminjaman.length > 0
              ? ListView.builder(
                  itemCount: _listPeminjaman.length,
                  itemBuilder: (context, index) {
                    Peminjaman peminjaman = _listPeminjaman[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Row(
                        children: [
                          Text('Id Peminjaman: '),
                          Text(peminjaman.idPeminjaman ?? ''),
                        ],
                      ),
                      subtitle: Row(
                        children: [
                          Text('Aset Dipinjam: '),
                          Text(peminjaman.namaAset ?? ''),
                        ],
                      ),
                      trailing: IconButton(
                          onPressed: () => showOption(peminjaman),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdatePeminjaman())
                  ?.then((value) => getPeminjaman()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}

class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const _defaultHeight = 56.0;

  final double elevation;
  final Gradient gradient;
  final Widget title;
  final double barHeight;

  GradientAppBar(
      {this.elevation = 3.0,
      required this.gradient,
      required this.title,
      this.barHeight = _defaultHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          offset: Offset(0, elevation),
          color: Colors.black.withOpacity(0.3),
          blurRadius: 3,
        ),
      ]),
      child: AppBar(
        title: title,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight);
}
