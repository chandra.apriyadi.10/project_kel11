import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/event/event_db.dart';
import 'package:project_si20b/screen/admin/add_update_aset.dart';

import '../../model/aset.dart';

class ListAset extends StatefulWidget {
  @override
  State<ListAset> createState() => _ListAsetState();
}

class _ListAsetState extends State<ListAset> {
  List<Aset> _listAset = [];

  void getAset() async {
    _listAset = await EventDb.getAset();

    setState(() {});
  }

  @override
  void initState() {
    getAset();
    super.initState();
  }

  void showOption(Aset? aset) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateAset(aset: aset))?.then((value) => getAset());
        break;
      case 'delete':
        EventDb.deleteAset(aset!.idAset!).then((value) => getAset());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Aset'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listAset.length > 0
              ? ListView.builder(
                  itemCount: _listAset.length,
                  itemBuilder: (context, index) {
                    Aset aset = _listAset[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(aset.namaAset ?? ''),
                      subtitle: Row(
                        children: [
                          Text('Persediaan: '),
                          Text(aset.jumlahAset ?? ''),
                        ],
                      ),
                      trailing: IconButton(
                          onPressed: () => showOption(aset),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdateAset())?.then((value) => getAset()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}

class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const _defaultHeight = 56.0;

  final double elevation;
  final Gradient gradient;
  final Widget title;
  final double barHeight;

  GradientAppBar(
      {this.elevation = 3.0,
      required this.gradient,
      required this.title,
      this.barHeight = _defaultHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          offset: Offset(0, elevation),
          color: Colors.black.withOpacity(0.3),
          blurRadius: 3,
        ),
      ]),
      child: AppBar(
        title: title,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight);
}
