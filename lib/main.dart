import 'package:flutter/material.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:project_si20b/config/asset.dart';
import 'package:project_si20b/model/user.dart';
import 'package:project_si20b/screen/admin/dashboard_admin.dart';
import 'package:project_si20b/screen/admin/home_screen.dart';
import 'package:project_si20b/screen/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: Asset.colorPrimaryDark,
        scaffoldBackgroundColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(builder: (context, AsyncSnapshot<User?> snapshot) {
        return Login();
      }),
    );
  }
}
