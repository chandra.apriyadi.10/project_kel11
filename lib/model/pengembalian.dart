class Pengembalian {
  String? idPengembalian;
  String? idPeminjaman;
  String? idUser;
  String? name;
  String? tanggalKembali;
  String? sudahDikembalikan;

  Pengembalian({
    this.idPengembalian,
    this.idPeminjaman,
    this.idUser,
    this.name,
    this.tanggalKembali,
    this.sudahDikembalikan,
  });

  factory Pengembalian.fromJson(Map<String, dynamic> json) => Pengembalian(
        idPengembalian: json['id_pengembalian'],
        idPeminjaman: json['id_peminjaman'],
        idUser: json['id_user'],
        name: json['name'],
        tanggalKembali: json['tanggal_kembali'],
        sudahDikembalikan: json['sudah_dikembalikan'],
      );

  Map<String, dynamic> toJson() => {
        'id_pengembalian': this.idPengembalian,
        'id_peminjaman': this.idPeminjaman,
        'id_user': this.idUser,
        'name': this.name,
        'tanggal_kembali': this.tanggalKembali,
        'sudah_dikembalikan': this.sudahDikembalikan,
      };
}
