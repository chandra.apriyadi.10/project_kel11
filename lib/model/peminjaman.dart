class Peminjaman {
  String? idPeminjaman;
  String? idUser;
  String? name;
  String? idAset;
  String? namaAset;
  String? tanggalPinjam;
  String? persetujuan;

  Peminjaman({
    this.idPeminjaman,
    this.idUser,
    this.name,
    this.idAset,
    this.namaAset,
    this.tanggalPinjam,
    this.persetujuan,
  });

  factory Peminjaman.fromJson(Map<String, dynamic> json) => Peminjaman(
        idPeminjaman: json['id_peminjaman'],
        idUser: json['id_user'],
        name: json['name'],
        idAset: json['id_aset'],
        namaAset: json['nama_aset'],
        tanggalPinjam: json['tanggal_pinjam'],
        persetujuan: json['persetujuan'],
      );

  Map<String, dynamic> toJson() => {
        'id_peminjaman': this.idPeminjaman,
        'id_user': this.idUser,
        'name': this.name,
        'id_aset': this.idAset,
        'nama_aset': this.namaAset,
        'tanggal_pinjam': this.tanggalPinjam,
        'persetujuan': this.persetujuan,
      };
}
