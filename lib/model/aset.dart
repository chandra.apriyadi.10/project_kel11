class Aset {
  String? idAset;
  String? namaAset;
  String? jumlahAset;

  Aset({
    this.idAset,
    this.namaAset,
    this.jumlahAset,
  });

  factory Aset.fromJson(Map<String, dynamic> json) => Aset(
        idAset: json['id_aset'],
        namaAset: json['nama_aset'],
        jumlahAset: json['jumlah_aset'],
      );

  Map<String, dynamic> toJson() => {
        'id_aset': this.idAset,
        'nama_aset': this.namaAset,
        'jumlah_aset': this.jumlahAset,
      };
}
