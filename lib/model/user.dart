class User {
  String? idUser;
  String? name;
  String? userName;
  String? pass;
  String? role;
  String? alamat;
  String? prodi;
  String? hp;

  User({
    this.idUser,
    this.name,
    this.userName,
    this.pass,
    this.role,
    this.alamat,
    this.prodi,
    this.hp,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        idUser: json['id_user'],
        name: json['name'],
        userName: json['username'],
        pass: json['pass'],
        role: json['role'],
        alamat: json['alamat'],
        prodi: json['prodi'],
        hp: json['hp'],
      );

  Map<String, dynamic> toJson() => {
        'id_user': this.idUser,
        'name': this.name,
        'username': this.userName,
        'pass': this.pass,
        'role': this.role,
        'alamat': this.alamat,
        'prodi': this.prodi,
        'hp': this.hp,
      };
}
