class Api {
  static const _host = "http://192.168.158.227/api_kel_11";

  static String _user = "$_host/user";
  static String _mahasiswa = "$_host/mahasiswa";
  static String _aset = "$_host/aset";
  static String _peminjaman = "$_host/peminjaman";
  static String _pengembalian = "$_host/pengembalian";
  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // mahasiswa
  static String addMahasiswa = "$_mahasiswa/add_mahasiswa.php";
  static String deleteMahasiswa = "$_mahasiswa/delete_mahasiswa.php";
  static String getMahasiswa = "$_mahasiswa/get_mahasiswa.php";
  static String updateMahasiswa = "$_mahasiswa/update_mahasiswa.php";

  //aset
  static String addAset = "$_aset/add_aset.php";
  static String deleteAset = "$_aset/delete_aset.php";
  static String getAset = "$_aset/get_aset.php";
  static String updateAset = "$_aset/update_aset.php";

  //peminjaman
  static String addPeminjaman = "$_peminjaman/add_peminjaman.php";
  static String deletePeminjaman = "$_peminjaman/delete_peminjaman.php";
  static String getPeminjaman = "$_peminjaman/get_peminjaman.php";
  static String updatePeminjaman = "$_peminjaman/update_peminjaman.php";

  //pengembalian
  static String addPengembalian = "$_pengembalian/add_pengembalian.php";
  static String deletePengembalian = "$_pengembalian/delete_pengembalian.php";
  static String getPengembalian = "$_pengembalian/get_pengembalian.php";
  static String updatePengembalian = "$_pengembalian/update_pengembalian.php";
}
