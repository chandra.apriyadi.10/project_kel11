import 'package:flutter/material.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 115, 18, 32);
  static Color colorPrimary = Color.fromARGB(255, 35, 133, 163);
  static Color colorSecoundary = Color.fromARGB(255, 46, 5, 112);
  static Color colorAccent = Color.fromARGB(255, 183, 180, 0);
}
