import 'package:get/get.dart';
import 'package:project_si20b/model/pengembalian.dart';

class CPengembalian extends GetxController {
  Rx<Pengembalian> _pengembalian = Pengembalian().obs;

  Pengembalian get pengembalian => _pengembalian.value;

  void setPengembalian(Pengembalian dataPengembalian) =>
      _pengembalian.value = dataPengembalian;
}
