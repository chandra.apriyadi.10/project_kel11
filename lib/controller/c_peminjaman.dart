import 'package:get/get.dart';
import 'package:project_si20b/model/peminjaman.dart';

class CPeminjaman extends GetxController {
  Rx<Peminjaman> _peminjaman = Peminjaman().obs;

  Peminjaman get peminjaman => _peminjaman.value;

  void setPeminjaman(Peminjaman dataPeminjaman) =>
      _peminjaman.value = dataPeminjaman;
}
