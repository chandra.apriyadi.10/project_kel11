import 'package:get/get.dart';
import 'package:project_si20b/model/aset.dart';

class CAset extends GetxController {
  Rx<Aset> _aset = Aset().obs;

  Aset get aset => _aset.value;

  void setAset(Aset dataAset) => _aset.value = dataAset;
}
